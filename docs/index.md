# 경사 하강 최적화 기법 <sup>[1](#footnote_1)</sup>

> <font size="3">신경망 훈련을 위해 경사 하강 최적화 기법을 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./gradient-descent-optimization.md#intro)
1. [경사 하강 알고리즘](./gradient-descent-optimization.md#sec_02)
1. [경사 하강의 변형](./gradient-descent-optimization.md#sec_03)
1. [학습률 선택](./gradient-descent-optimization.md#sec_04)
1. [운동량과 네스테로프 가속 경사](./gradient-descent-optimization.md#sec_05)
1. [적응 경사법](./gradient-descent-optimization.md#sec_06)
1. [확률적 경사 하강법과 미니 배치 경사 하강법](./gradient-descent-optimization.md#sec_07)
1. [배치 정규화와 그래디언트 클리핑](./gradient-descent-optimization.md#sec_08)
1. [마치며](./gradient-descent-optimization.md#summary)

<a name="footnote_1">1</a>: [ML Tutorial 12 — Gradient Descent Optimization Techniques](https://ai.plainenglish.io/ml-tutorial-12-gradient-descent-optimization-techniques-d3b256124b4e?sk=2052b2a0f52d17c937d3a05eb48974b6)을 편역하였습니다.
